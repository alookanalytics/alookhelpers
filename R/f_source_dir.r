#' Function to source all functions in a folder
#'
#' Iteratively sources all functions contained in a folder
#'
#' @param path to a folder with R scripts
#' @param trace logical, \code{TRUE} to write sourced functions to console
#' @param subfolders logical, \code{TRUE} to source functions in all subfolders
#' @keywords source
#' @export
#' @examples
#' f_source_dir('./helpers')
f_source_dir <- function(path, trace = TRUE, subfolders = TRUE, ...) {
  for (nm in list.files(path, pattern = '[.][RrSsQq]$')) {
    if(trace) cat(nm, ':')
    source(file.path(path, nm), ...)
    if(trace) cat('\n')
  }
  
  if(subfolders){
    folders <- list.dirs(path, recursive=F, full.names=T)
    if(length(folders) > 0){
      for (i in 1:length(folders)){
        if(trace) cat(folders[i], '\n')
        f_source_dir(folders[i], trace, subfolders, ...)
      }
    }
  }
}