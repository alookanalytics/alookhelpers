#' Function to properly load packages
#'
#' Checks the packages needed and install those that are not available.
#'
#' @param list_of_packages vector with packages to be loaded
#' @keywords loading packages
#' @export
#' @examples
#' f_load_libraries('foo')
f_load_libraries <- function(list_of_packages){
  
  #' check which are not already installed
  new_packages <- list_of_packages[!(list_of_packages %in% installed.packages()[, 'Package'])]
  #' install only missing packages
  if(length(new_packages)) install.packages(new_packages, repos = 'http://cran.us.r-project.org')
  #' and load all of them
  lapply(list_of_packages, function(x){suppressPackageStartupMessages(library(x, character.only = TRUE))})
  
  #' error handling
  if(sum(!(list_of_packages %in% installed.packages()[, 'Package'])) > 0){
    stop('Libraries not loaded.')
  } else {
    cat(paste0('[R SCRIPT PROGRESS @', Sys.time(),' (aLookHelpers::f_load_libraries)]: Libraries loaded.\n'))
  }
  
  #' clear help variables
  rm(list_of_packages, new_packages)
  
}