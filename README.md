# aLookHelpers #

Miscellaneous R helper functions useful for aLook Analytics team.

# Install #

Run to install and load the current version

```r
devtools::install_bitbucket(repo='alookanalytics/aLookHelpers')
library(aLookHelpers)
```